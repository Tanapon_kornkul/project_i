using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Manager
{
    public class SoundManager : MonoBehaviour
    {
        [SerializeField] private SoundClip[] soundClips;
        

        public static SoundManager Instance { get; private set; }
        private AudioSource audioSource;
        
        public void Awake()
        {
            Debug.Assert(soundClips != null && soundClips.Length != 0, "Sound clips need to be setup");
            audioSource = GetComponent<AudioSource>(); 
            
            if (Instance == null)
            {
                Instance = this;
            }
            DontDestroyOnLoad(this);
        }

        public enum Sound
        {
            BGM,
            PlayerFire, 
            //Explod
        }
        
        [Serializable]
        public struct SoundClip
        {
            public Sound sound;
            public AudioClip AudioClip;
            [Range(0,1)] public float soundVolume;
        }

        
        
        public void Play(AudioSource audioSource, Sound sound)
        {
            var soundClip = GetSoundClip(sound);
            audioSource.clip = soundClip.AudioClip;
            audioSource.volume = soundClip.soundVolume;
            audioSource.Play();
        }

        public void PlayBGM()
        { 
            audioSource.loop = true;
            Play(audioSource,Sound.BGM);
        }
        
        private SoundClip GetSoundClip(Sound sound)
        {
            foreach (var soundClip in soundClips)
            {
                if (soundClip.sound == sound)
                {
                    return soundClip; 
                }
            }
            
            return default(SoundClip);
        }
    }
}
