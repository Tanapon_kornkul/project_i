﻿using System;
using System.Collections;
using System.Collections.Generic;
using Manager;
using Spaceship;
using UnityEngine;

namespace EnemyShip
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private float chasingThresholdDistance;

        private PlayerSpaceship spawnedPlayership;

        //public void Init(PlayerSpaceship playerSpaceship)
        //{
        //    spawnedPlayership = playerSpaceship; 
       // }

       private void Awake()
       {
           spawnedPlayership = GameManager.Instance.spawnedPlayership;
       }

       private void Update()
        {
            MoveToPlayer();
            enemySpaceship.Fire();
        }

        private void MoveToPlayer()
        {
            if (spawnedPlayership != null)
            {
                var distanceToPlayer = Vector2.Distance(spawnedPlayership.transform.position, transform.position);
                //Debug.Log(distanceToPlayer);
                if (distanceToPlayer < chasingThresholdDistance)
                {
                    var direction = (Vector2) (spawnedPlayership.transform.position - transform.position);
                    direction.Normalize();
                    var distance = direction * enemySpaceship.Speed * Time.deltaTime;
                    gameObject.transform.Translate(distance);
                }

            }
            
            
            /*private void MoveToPlayer()
            {
                
            }*/

        }
    }
}





