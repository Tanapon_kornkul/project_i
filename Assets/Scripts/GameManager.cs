﻿using System;
using EnemyShip;
using Spaceship;
using TMPro;
using UnityEditor.SearchService;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Spaceship;

namespace Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private Button startButton;
        [SerializeField] private Button restartButton;
        [SerializeField] private Button quitButton;
        [SerializeField] private RectTransform menuDialog;
        [SerializeField] private RectTransform showDialog;
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private ScoreManager scoreManager;
        public event Action OnRestarted;
        private int playerSpaceshipHp = 2000;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemySpaceshipMoveSpeed;
        
        [SerializeField] private AudioClip playersoundExp;
        
        [SerializeField] private TextMeshProUGUI PlayerHP;

        //[SerializeField] private SoundManager soundManager;
        
        public PlayerSpaceship spawnedPlayership;
        
        public static GameManager Instance { get; private set; }
        private void Awake()
        {
            Debug.Assert(startButton != null,"StartButton cannot be null");
            Debug.Assert(menuDialog != null,"Dialog cannot be null");
            Debug.Assert(playerSpaceship != null,"PlayerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null,"EnemySpaceship cannot be null");
            //Debug.Assert(scoreManager != null,"ScoreManager cannot be null");
            Debug.Assert(playerSpaceshipHp > 0,"PlayerSpaceshipHp cannot be null");
            Debug.Assert(playerSpaceshipMoveSpeed > 0,"PlayerSpaceshipMoveSpeed cannot be null");
            Debug.Assert(enemySpaceshipHp > 0,"EnemySpaceshipHp cannot be null");
            Debug.Assert(enemySpaceshipMoveSpeed > 0,"EnemySpaceshipMoveSpeed cannot be null");

            startButton.onClick.AddListener(OnStartButtonClicked);
            quitButton.onClick.AddListener(OnQuitButton);
            //soundManager.PlayBGM();

            if (Instance == null)
            {
                Instance = this;
            }
            DontDestroyOnLoad(this);

        }

        public void Update()
        {
            
        }

        public void Hpset(int Hp)
        {
            PlayerHP.text = $"Player Hp : {Hp}";
            
        } 

        private void OnStartButtonClicked()
        {
            menuDialog.gameObject.SetActive(false);
            StartGame();
        }

        private void OnQuitButton()
        {
            Application.Quit();
        }

        private void StartGame()
        {
            scoreManager.Init(this);
            SpawnPlayerSpaceship();
            
            SpawnEnemySpaceship(-3,8,3);
            SpawnEnemySpaceship(-2,8,3);
            SpawnEnemySpaceship(-3,8,3);
            SpawnEnemySpaceship(-2,8,3);
            
            SpawnEnemySpaceship(3,8,11);
            SpawnEnemySpaceship(4, 8, 11 );
            SpawnEnemySpaceship(3, 8, 11 );
            SpawnEnemySpaceship(4, 8, 11 );
            
            

            SoundManager.Instance.PlayBGM();  
        }
        

        private void SpawnPlayerSpaceship()
        {
            spawnedPlayership = Instantiate(playerSpaceship);
            spawnedPlayership.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spawnedPlayership.OnExploded += OnPlayerSpaceshipExploded;
        }

        private void OnPlayerSpaceshipExploded()
        {
            showDialog.gameObject.SetActive(true);
            restartButton.onClick.AddListener(Restart);
            AudioSource.PlayClipAtPoint(playersoundExp, Camera.main.transform.position, 2);
            //SceneManager.LoadScene("Game2");
            //Restart();
        }

        private void SpawnEnemySpaceship(int spawnEnemy, int spawnEnemy2 , int spawnEnemy3)
        {
            var spawnedEnemyShip = Instantiate( enemySpaceship, new Vector3 (spawnEnemy,spawnEnemy2,0), 
                Quaternion.identity);
            spawnedEnemyShip.Init(enemySpaceshipHp,enemySpaceshipMoveSpeed);
            spawnedEnemyShip.OnExploded += OnEnemySpaceshipExploded;
            
            //var enemyController = spawnedEnemyShip.GetComponent<EnemyController>();
            //enemyController.Init(spawnedPlayership); 
        }
        
        
        private void OnEnemySpaceshipExploded()
        {
            AudioSource.PlayClipAtPoint(playersoundExp, Camera.main.transform.position, 2);
            scoreManager.SetScore(1);
            Round2();
            //Nextscene();
            //SceneManager.LoadScene("Game2");
            //showDialog.gameObject.SetActive(true);
            //restartButton.onClick.AddListener(Restart);
        }

        

        private void Restart()
        {
            
            DestroyRemainingShip();
            menuDialog.gameObject.SetActive(true);
            showDialog.gameObject.SetActive(false);
            OnRestarted?.Invoke();
            Update();
        }

        private void DestroyRemainingShip()
        {
            var remainingEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in remainingEnemies)
            {
                Destroy(enemy);
            }
            
            var remainingPlayers = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in remainingPlayers)
            {
                Destroy(player);
            }
        }

        public void Round2()
        {
            var remainingEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            if (remainingEnemies.Length <= 0)
            {
                SpawnEnemySpaceship(2,10,3);
                SpawnEnemySpaceship(3,8,2);

                SpawnEnemySpaceship(-1,8,3);
                SpawnEnemySpaceship(-9,9,2);
            }
        }
        

        /*public void Nextscene()
        {
            var remainingEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            if (remainingEnemies.Length <= 0)
            {
                SceneManager.LoadScene("Game2");
            }*/
        }
    }
